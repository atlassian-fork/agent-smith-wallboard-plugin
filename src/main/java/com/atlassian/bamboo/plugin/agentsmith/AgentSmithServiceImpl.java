package com.atlassian.bamboo.plugin.agentsmith;

import com.amazonaws.services.ec2.model.Instance;
import com.atlassian.aws.AWSAccount;
import com.atlassian.aws.ec2.InstancePaymentType;
import com.atlassian.aws.ec2.InstanceStatus;
import com.atlassian.aws.ec2.RemoteEC2Instance;
import com.atlassian.bamboo.agent.elastic.aws.AwsAccountBean;
import com.atlassian.bamboo.agent.elastic.server.ElasticAccountManagementService;
import com.atlassian.bamboo.agent.elastic.server.ElasticImageConfiguration;
import com.atlassian.bamboo.agent.elastic.server.ElasticInstanceManager;
import com.atlassian.bamboo.agent.elastic.server.RemoteElasticInstance;
import com.atlassian.bamboo.agent.elastic.server.RemoteElasticInstanceState;
import com.atlassian.bamboo.build.BuildExecutionManager;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.plan.ExecutionStatus;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Agent;
import com.atlassian.bamboo.plugin.agentsmith.statistic.ElasticProperties;
import com.atlassian.bamboo.plugin.agentsmith.statistic.QueuedJob;
import com.atlassian.bamboo.plugin.agentsmith.statistic.Statistics;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.CurrentlyBuilding;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.queue.BuildQueueManager;
import com.atlassian.bamboo.ww2.actions.admin.elastic.ElasticUIBean;
import com.google.common.base.Predicate;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Status;
import static com.atlassian.bamboo.plugin.agentsmith.statistic.Agent.Type;
import static com.atlassian.bamboo.plugin.agentsmith.statistic.ElasticProperties.ElasticStatus;
import com.atlassian.bamboo.v2.build.queue.QueueManagerView;
import com.atlassian.fugue.Iterables;

/**
 * Default implementation providing statistics on Bamboo.
 */
public class AgentSmithServiceImpl implements AgentSmithService {
    private static final Logger logger = LoggerFactory.getLogger(AgentSmithServiceImpl.class);
    private final AgentManager agentManager;
    private final ElasticInstanceManager elasticInstanceManager;
    private final BuildExecutionManager buildExecutionManager;
    private final BuildQueueManager buildQueueManager;
    private final ElasticUIBean elasticUIBean;
    private final AwsAccountBean awsAccountBean;
    private final ElasticAccountManagementService elasticAccountManagementService;
    private final LoadingCache<String, Collection<Instance>> disconnectedInstancesCache;
    private final LoadingCache<String, Statistics> statisticsCache;
    private static final String STATISTICS_KEY = "statisticsKey";

    /**
     * Creates a statistic service.
     *
     * @param agentManager           Service managing the agents.
     * @param elasticInstanceManager Service managing the elastic agents.
     * @param buildExecutionManager  Service managing the build execution.
     * @param buildQueueManager      Service managing the build queue.
     * @param elasticUIBean          Elastic agents details.
     * @param awsAccountBean         Amazon Web Services account details.
     * @param elasticAccountManagementService
     */
    public AgentSmithServiceImpl(AgentManager agentManager, ElasticInstanceManager elasticInstanceManager,
                                 BuildExecutionManager buildExecutionManager,
                                 BuildQueueManager buildQueueManager, ElasticUIBean elasticUIBean,
                                 AwsAccountBean awsAccountBean, ElasticAccountManagementService elasticAccountManagementService) {
        this.agentManager = agentManager;
        this.elasticInstanceManager = elasticInstanceManager;
        this.buildExecutionManager = buildExecutionManager;
        this.buildQueueManager = buildQueueManager;
        this.elasticUIBean = elasticUIBean;
        this.awsAccountBean = awsAccountBean;
        this.elasticAccountManagementService = elasticAccountManagementService;
        this.disconnectedInstancesCache = CacheBuilder.<String, Collection<Instance>>newBuilder()
                .expireAfterWrite(1, TimeUnit.HOURS)
                .build(new CacheLoader<String, Collection<Instance>>() {

            @Override
            public Collection<Instance> load(String key) throws Exception {
                Collection<Instance> result = Sets.newHashSet();
                try {
                    result.addAll(AgentSmithServiceImpl.this.elasticAccountManagementService.getDisconnectedElasticInstances());
                    result.addAll(AgentSmithServiceImpl.this.elasticAccountManagementService.getUnrelatedElasticInstances());
                }catch (Exception e) {
                    logger.debug("Failed to get disconnected ", e);
                }
                return result;
            }
        });
        statisticsCache = CacheBuilder.<String, Statistics>newBuilder()
                .expireAfterWrite(15, TimeUnit.SECONDS)
                .build(new CacheLoader<String, Statistics>() {
                    @Override
                    public Statistics load(String key) throws Exception {
                        logger.debug("Starting to gather statistics.");
                        Statistics statistics = new Statistics();
                        Map<Long, Agent> agents = getAgents();
                        Collection< RemoteElasticInstance> pendingElasticInstances = getPendingElasticInstances();
                        statistics.analyzePendingElasticInstances(pendingElasticInstances);
                        statistics.analyzeAgents(agents);
                        statistics.analyzeDisconnectedElasticInstances(getDisconnectedAwsInstances());
                        statistics.analyzeQueuedJobs(getQueueStatistics());
                        logger.debug("Finished to gather statistics.");

                        return statistics;
                    }
        });
    }

    @Override
    public Statistics getStatistics() {
        return statisticsCache.getUnchecked(STATISTICS_KEY);
    }

    private List<QueuedJob> getQueueStatistics() {
        List<QueuedJob> queuedJobs = new LinkedList<>();

        try {
            QueueManagerView<CommonContext, CommonContext> queue = QueueManagerView.newView(buildQueueManager, (BuildQueueManager.QueueItemView<CommonContext> input) -> input);
            for (BuildQueueManager.QueueItemView<CommonContext> qi : queue.getQueueView(Iterables.emptyIterable())) {
                CommonContext commonContext = qi.getView();
                try {
                    CurrentlyBuilding currentlyBuilding =
                            buildExecutionManager.getCurrentlyBuildingByPlanResultKey(commonContext.getResultKey());
                    if (currentlyBuilding == null)
                        continue;

                    String planName = commonContext.getDisplayName();
                    String planKey = commonContext.getResultKey().getKey();
                    Date queueTime = currentlyBuilding.getQueueTime();
                    QueuedJob queuedJob = new QueuedJob(planName, planKey, queueTime);
                    queuedJob.setAgentAvailability(getAgentAvailability(currentlyBuilding));
                    queuedJobs.add(queuedJob);
                } catch (Exception e) {
                    logger.error("Couldn't get statistics of '" + commonContext.getDisplayName() + "'", e);
                }
            }
        } catch (Exception e) {
            logger.error("Couldn't get statistics in the building queue.", e);
        }

        return queuedJobs;
    }

    private QueuedJob.AgentAvailability getAgentAvailability(final ExecutionStatus currentlyBuilding) {
        final PlanResultKey planResultKey = currentlyBuilding.getBuildIdentifier().getPlanResultKey();
        
        final Set<Long> executorsForQueuedExecutable = buildQueueManager.getExecutorsForQueuedExecutable(planResultKey);
        if (executorsForQueuedExecutable==null || !executorsForQueuedExecutable.isEmpty()) {
            return QueuedJob.AgentAvailability.EXECUTABLE;
        }
        
        final Collection<ElasticImageConfiguration> imagesForQueuedExecutable = buildQueueManager.getImagesForQueuedExecutable(planResultKey);
        if (imagesForQueuedExecutable==null) {
            return QueuedJob.AgentAvailability.EXECUTABLE;
        } else if (!imagesForQueuedExecutable.isEmpty()) {
            return QueuedJob.AgentAvailability.ELASTIC;
        } else {
            return QueuedJob.AgentAvailability.NONE;
        }
    }

    /**
     * Retrieves agents and their details.
     *
     * @return agents.
     */
    private Map<Long, Agent> getAgents() {
        Map<Long, Agent> agents = new HashMap<>();

        List<BuildAgent> localAgents = new ArrayList<>(agentManager.getAllLocalAgents());
        List<BuildAgent> remoteAgents = new ArrayList<>(agentManager.getAllRemoteAgents());

        try {
            //TODO: Replace the verification with a check using AdministrationConfigurationAccessor.
            if (getAwsAccount() != null) {
                List<BuildAgent> elasticRunningAgents =
                        new ArrayList<>(agentManager.getOnlineElasticAgents());
                remoteAgents.removeAll(elasticRunningAgents);

                for (Agent agent : createAgentsByType(elasticRunningAgents, Type.ELASTIC)) {
                    agents.put(agent.getId(), agent);
                }
                setElasticDetails(agents);
            }
        } catch (Exception e) {
            logger.error("Couldn't get the statistics on the elastic agents.", e);
        }

        try {
            for (Agent agent : createAgentsByType(remoteAgents, Type.REMOTE)) {
                agents.put(agent.getId(), agent);
            }
        } catch (Exception e) {
            logger.error("Couldn't get the statistics on the remote agents.", e);
        }

        try {
            for (Agent agent : createAgentsByType(localAgents, Type.LOCAL)) {
                agents.put(agent.getId(), agent);
            }
        } catch (Exception e) {
            logger.error("Couldn't get the statistics on the local agents.", e);
        }
        return agents;
    }

    /**
     * Creates a List of agents based on a list of BuildAgents.
     *
     * @param buildAgents Original build agent objects on which the agents objects will be built.
     * @param type        Type of agents.
     * @return the agents.
     */
    private List<Agent> createAgentsByType(List<BuildAgent> buildAgents, Type type) {
        List<Agent> agents = new ArrayList<>(buildAgents.size());

        for (BuildAgent buildAgent : buildAgents) {
            Agent agent = new Agent(buildAgent.getId(), type);

            if (!buildAgent.isActive()) {
                agent.setStatus(Status.OFFLINE);
            } else if (!buildAgent.isEnabled()) {
                agent.setStatus(Status.DISABLED);
            } else if (!buildAgent.isBusy()) {
                agent.setStatus(Status.IDLE);
            } else {
                CurrentlyBuilding currentlyBuilding =
                        buildExecutionManager.getBuildRunningOnAgent(buildAgent.getId());
                if (currentlyBuilding != null && currentlyBuilding.getBuildHangDetails() != null) {
                    agent.setStatus(Status.HUNG);
                } else {
                    agent.setStatus(Status.BUSY);
                }
            }

            // Create a helper to use implementation of BuildAgent.isDedicated(). Because the method is deprecated and there's no replacement.
            // Create a PR to bamboo to add a helper method AgentAssignmentHelper.isDedicatedAgent .
            // Todo add replace with the helper method once available in bamboo
            agent.setDedicated(AgentAssignmentHelper.isDedicatedAgent(buildAgent));
            agents.add(agent);
        }
        return agents;
    }

    /**
     * Retrieves additional data for Elastic agents.
     * <p>
     * {@link Agent} with the {@link Type} {@link Type#ELASTIC} will have their
     * {@link Agent#getElasticProperties()} set properly.
     * </p>
     *
     * @param agents map of every existing agents.
     */
    private void setElasticDetails(Map<Long, Agent> agents) {
        List<RemoteElasticInstance> connectedElasticInstances = elasticInstanceManager.getAllElasticRemoteAgents();

        for (RemoteElasticInstance elasticInstance : connectedElasticInstances) {
            try {
                Agent agent = agents.get(elasticInstance.getRemoteAgent());
                if (agent == null) {
                    logger.debug("The remote agent '{}' is provided by the ElasticInstanceManager"
                            + " but it doesn't contain a remote agent (probably pending or shutting down). ", elasticInstance.getInstance().getInstanceId());
                    continue;
                }

                ElasticProperties agentProperties = agent.getElasticProperties();

                /*
                 * Find the state.
                 */
                ElasticStatus elasticStatus = getElasticStatus(elasticInstance);
                agentProperties.setElasticStatus(elasticStatus);

                /*
                 * Add the EC2 properties.
                 */
                RemoteEC2Instance remoteEC2Instance = elasticInstance.getInstance();
                addEc2Properties(agentProperties, remoteEC2Instance);

                Double instanceCost = elasticUIBean.getInstancePrice(elasticInstance);
                if (instanceCost != null)
                    agentProperties.setCost(instanceCost);
            } catch (Exception e) {
                logger.error("Couldn't gather the elastic details for '{}'", elasticInstance.getRemoteAgent(), e);
            }
        }
    }

    /**
     * Find out the disconnected EC2 instances
     * @return Collection of disconnected EC2 instances
     */
    public Collection<Instance> getDisconnectedAwsInstances() {
        return disconnectedInstancesCache.getUnchecked("disconnected");
    }

    /**
     * Find the EC2 properties of the server on which the agent is running and add them to the elastic agent details.
     *
     * @param agentProperties   agent to update with the EC2 details.
     * @param remoteEC2Instance EC2 instance.
     */
    private void addEc2Properties(ElasticProperties agentProperties, RemoteEC2Instance remoteEC2Instance) {
        if (remoteEC2Instance != null) {
            InstanceStatus instanceStatus = remoteEC2Instance.getInstanceStatus();
            InstancePaymentType paymentType = getInstancePaymentType(remoteEC2Instance, InstancePaymentType.REGULAR);
            agentProperties.setPaymentType(paymentType);
            agentProperties.setElasticInstanceId(remoteEC2Instance.getInstanceId());

            if (instanceStatus.getLaunchTime() != null) {
                agentProperties.setLaunchTime(instanceStatus.getLaunchTime());
            }
        }
    }

    private InstancePaymentType getInstancePaymentType(final RemoteEC2Instance remoteEC2Instance, final InstancePaymentType defaultType) {
        final InstanceStatus instanceStatus = remoteEC2Instance.getInstanceStatus();
        final InstancePaymentType instancePaymentType = instanceStatus.getInstancePaymentType();
        if (instancePaymentType == null) {
            logger.info("Elastic instance with id {} has no payment type set. Using default value {} instead.", instanceStatus.getInstanceId(), defaultType);
            return defaultType;
        } else {
            return instancePaymentType;
        }
    }

    /**
     * Determines the status of an elastic instance for statistic purposes.
     *
     * @param elasticInstance actual status of the elastic instance.
     * @return the elastic status used for statistical purposes.
     */
    private ElasticStatus getElasticStatus(RemoteElasticInstance elasticInstance) {
        ElasticStatus elasticStatus;
        RemoteElasticInstanceState state = elasticInstance.getState();
        switch (state) {
            case RUNNING:
                elasticStatus = ElasticStatus.RUNNING;
                break;
            case INITIAL:
            case BIDDING:
            case STARTING:
            case IDENTIFIED:
                // This should not be reachable according to current logic in bamboo.
                // https://stash.atlassian.com/projects/BAM/repos/bamboo/browse/components/bamboo-web-app/src/main/webapp/admin/elastic/commonElasticFunctions.ftl#112
                elasticStatus = ElasticStatus.PENDING;
                break;
            case STOPPING:
            case STOPPED:
            case SHUTTING_DOWN:
                elasticStatus = ElasticStatus.SHUTTING_DOWN;
                break;
            case TERMINATED:
            case FAILED_TO_START:
            case UNKNOWN:
                elasticStatus = ElasticStatus.FAILED;
                break;
            default:
                throw new IllegalArgumentException("The remote instance state '" + state + "' isn't handled.");
        }
        return elasticStatus;
    }

    /**
     * Gets the Amazon Web Services account.
     *
     * @return the AWS account or null if it isn't accessible.
     */
    private AWSAccount getAwsAccount() {
        AWSAccount account = null;
        try {
            account = awsAccountBean.getAwsAccount();
        } catch (Exception e) {
            logger.info("Unable to get aws info, assuming AWS is disabled");
        }
        return account;
    }

    /**
     * Get ec2 instances who are pending or whose agents are pending
     * @return
     */
    private Collection<RemoteElasticInstance> getPendingElasticInstances() {
        return  Collections2.filter(elasticInstanceManager.getElasticRemoteAgents(), new Predicate<RemoteElasticInstance>() {
            @Override
            public boolean apply(final RemoteElasticInstance instance)  {
                RemoteElasticInstanceState state = instance.getState();

                return state == RemoteElasticInstanceState.IDENTIFIED
                        || state == RemoteElasticInstanceState.STARTING
                        || state == RemoteElasticInstanceState.INITIAL
                        || (agentManager.getAgent(instance.getRemoteAgent()) == null && instance.isAgentLoading());
            }
        });
    }
}
